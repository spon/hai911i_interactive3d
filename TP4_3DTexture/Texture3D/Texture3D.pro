# -------------------------------------------------
# Project created by QtCreator 2010-01-27T15:21:45
# -------------------------------------------------
QT += xml
QT += opengl
TARGET = texture3D
TEMPLATE = app
MOC_DIR = ./moc
OBJECTS_DIR = ./obj
#DEPENDPATH += ./GLSL
#INCLUDEPATH += ./GLSL
#DEPENDPATH += /usr/local/include/QGLViewer
#INCLUDEPATH += /usr/local/include/QGLViewer
#LIB_DIR += /usr/local/lib/

SOURCES += Main.cpp \
    Mesh.cpp \
    Window.cpp \
    TextureViewer.cpp \
    Texture.cpp \
    TextureDockWidget.cpp

HEADERS += Window.h \
    Mesh.h \
    TextureViewer.h \
    Texture.h \
    TextureDockWidget.h \
    Vec3.h \
    Vec3D.h

LIBS = -L/usr/local/lib/ -lQGLViewer-qt5 \
    -lglut \
    -lGLU
