#version 430


// --------------------------------------------------
// shader definition
// --------------------------------------------------


uniform sampler3D Mask; // d�claration de la map mask

uniform float xCutPosition;
uniform float yCutPosition;
uniform float zCutPosition;

uniform int xCutDirection; 
uniform int yCutDirection;
uniform int zCutDirection;

in vec3 position;
in vec3 textCoord;

out vec4 fragColor;

bool ComputeVisibility(vec3 point){
	vec3 xCut = vec3(xCutPosition, 0.,0.);
	vec3 yCut = vec3(0., yCutPosition,0.);
	vec3 zCut = vec3(0., 0.,zCutPosition);

	if(dot(xCut * xCutDirection, vec3(point - xCut)) < 0)
		return false;

	if(dot(yCut * yCutDirection, vec3(point - yCut)) < 0)
		return false;

	if(dot(zCut * zCutDirection, vec3(point - zCut)) < 0)
		return false;

	return true; 
}

// --------------------------------------------------
// Fragment Shader:
// --------------------------------------------------
void main() {

	if(!ComputeVisibility(position)) {
		discard;
	}

//TODO fetch color in texture
        //fragColor = vec4( 1., 0.,0.,1. );
        fragColor = texture(Mask, textCoord);
	
}
