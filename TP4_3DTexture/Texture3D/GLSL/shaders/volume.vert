// --------------------------------------------------
// shader definition
// --------------------------------------------------
#version 430

layout (location=0) in vec3 i_position;

// --------------------------------------------------
// Uniform variables:
// --------------------------------------------------
	uniform float xCutPosition;
	uniform float yCutPosition;
	uniform float zCutPosition;

	uniform float xMax;
	uniform float yMax;
	uniform float zMax;

	uniform float xMin;
	uniform float yMin;
	uniform float zMin;

	uniform mat4 mv_matrix;
	uniform mat4 proj_matrix;

// --------------------------------------------------
// varying variables
// --------------------------------------------------
        out vec3 position;
        out vec3 textCoord;
// --------------------------------------------------
// Vertex-Shader
// --------------------------------------------------


//TODO compute texture coord	
void main()
{
    position = vec3(i_position.x, i_position.y, i_position.z);
	textCoord = vec3( (position.x - xMin) / (xMax - xMin) , (position.y - yMin) / (yMax - yMin), (position.z - zMin)/ (zMax - zMin));
	gl_Position = proj_matrix * mv_matrix * vec4(i_position.xyz, 1.0);
}
