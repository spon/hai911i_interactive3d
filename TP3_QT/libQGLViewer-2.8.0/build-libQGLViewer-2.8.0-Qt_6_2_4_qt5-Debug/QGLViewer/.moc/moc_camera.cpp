/****************************************************************************
** Meta object code from reading C++ file 'camera.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.2.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../QGLViewer/camera.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'camera.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.2.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_qglviewer__Camera_t {
    const uint offsetsAndSize[164];
    char stringdata0[1002];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_qglviewer__Camera_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_qglviewer__Camera_t qt_meta_stringdata_qglviewer__Camera = {
    {
QT_MOC_LITERAL(0, 17), // "qglviewer::Camera"
QT_MOC_LITERAL(18, 11), // "setPosition"
QT_MOC_LITERAL(30, 0), // ""
QT_MOC_LITERAL(31, 3), // "Vec"
QT_MOC_LITERAL(35, 3), // "pos"
QT_MOC_LITERAL(39, 14), // "setOrientation"
QT_MOC_LITERAL(54, 10), // "Quaternion"
QT_MOC_LITERAL(65, 1), // "q"
QT_MOC_LITERAL(67, 5), // "theta"
QT_MOC_LITERAL(73, 3), // "phi"
QT_MOC_LITERAL(77, 11), // "setUpVector"
QT_MOC_LITERAL(89, 2), // "up"
QT_MOC_LITERAL(92, 6), // "noMove"
QT_MOC_LITERAL(99, 16), // "setViewDirection"
QT_MOC_LITERAL(116, 9), // "direction"
QT_MOC_LITERAL(126, 6), // "lookAt"
QT_MOC_LITERAL(133, 6), // "target"
QT_MOC_LITERAL(140, 15), // "showEntireScene"
QT_MOC_LITERAL(156, 9), // "fitSphere"
QT_MOC_LITERAL(166, 6), // "center"
QT_MOC_LITERAL(173, 6), // "radius"
QT_MOC_LITERAL(180, 14), // "fitBoundingBox"
QT_MOC_LITERAL(195, 3), // "min"
QT_MOC_LITERAL(199, 3), // "max"
QT_MOC_LITERAL(203, 15), // "fitScreenRegion"
QT_MOC_LITERAL(219, 9), // "rectangle"
QT_MOC_LITERAL(229, 11), // "centerScene"
QT_MOC_LITERAL(241, 24), // "interpolateToZoomOnPixel"
QT_MOC_LITERAL(266, 5), // "pixel"
QT_MOC_LITERAL(272, 21), // "interpolateToFitScene"
QT_MOC_LITERAL(294, 13), // "interpolateTo"
QT_MOC_LITERAL(308, 5), // "Frame"
QT_MOC_LITERAL(314, 2), // "fr"
QT_MOC_LITERAL(317, 8), // "duration"
QT_MOC_LITERAL(326, 7), // "setType"
QT_MOC_LITERAL(334, 4), // "Type"
QT_MOC_LITERAL(339, 4), // "type"
QT_MOC_LITERAL(344, 14), // "setFieldOfView"
QT_MOC_LITERAL(359, 3), // "fov"
QT_MOC_LITERAL(363, 24), // "setHorizontalFieldOfView"
QT_MOC_LITERAL(388, 4), // "hfov"
QT_MOC_LITERAL(393, 16), // "setFOVToFitScene"
QT_MOC_LITERAL(410, 14), // "setAspectRatio"
QT_MOC_LITERAL(425, 6), // "aspect"
QT_MOC_LITERAL(432, 23), // "setScreenWidthAndHeight"
QT_MOC_LITERAL(456, 5), // "width"
QT_MOC_LITERAL(462, 6), // "height"
QT_MOC_LITERAL(469, 19), // "setZNearCoefficient"
QT_MOC_LITERAL(489, 4), // "coef"
QT_MOC_LITERAL(494, 23), // "setZClippingCoefficient"
QT_MOC_LITERAL(518, 14), // "setSceneRadius"
QT_MOC_LITERAL(533, 14), // "setSceneCenter"
QT_MOC_LITERAL(548, 23), // "setSceneCenterFromPixel"
QT_MOC_LITERAL(572, 19), // "setSceneBoundingBox"
QT_MOC_LITERAL(592, 13), // "setPivotPoint"
QT_MOC_LITERAL(606, 5), // "point"
QT_MOC_LITERAL(612, 22), // "setPivotPointFromPixel"
QT_MOC_LITERAL(635, 21), // "setRevolveAroundPoint"
QT_MOC_LITERAL(657, 30), // "setRevolveAroundPointFromPixel"
QT_MOC_LITERAL(688, 8), // "setFrame"
QT_MOC_LITERAL(697, 23), // "ManipulatedCameraFrame*"
QT_MOC_LITERAL(721, 3), // "mcf"
QT_MOC_LITERAL(725, 23), // "setKeyFrameInterpolator"
QT_MOC_LITERAL(749, 1), // "i"
QT_MOC_LITERAL(751, 21), // "KeyFrameInterpolator*"
QT_MOC_LITERAL(773, 3), // "kfi"
QT_MOC_LITERAL(777, 17), // "addKeyFrameToPath"
QT_MOC_LITERAL(795, 8), // "playPath"
QT_MOC_LITERAL(804, 10), // "deletePath"
QT_MOC_LITERAL(815, 9), // "resetPath"
QT_MOC_LITERAL(825, 12), // "drawAllPaths"
QT_MOC_LITERAL(838, 11), // "setFlySpeed"
QT_MOC_LITERAL(850, 5), // "speed"
QT_MOC_LITERAL(856, 13), // "setIODistance"
QT_MOC_LITERAL(870, 8), // "distance"
QT_MOC_LITERAL(879, 27), // "setPhysicalDistanceToScreen"
QT_MOC_LITERAL(907, 22), // "setPhysicalScreenWidth"
QT_MOC_LITERAL(930, 16), // "setFocusDistance"
QT_MOC_LITERAL(947, 18), // "initFromDOMElement"
QT_MOC_LITERAL(966, 11), // "QDomElement"
QT_MOC_LITERAL(978, 7), // "element"
QT_MOC_LITERAL(986, 15) // "onFrameModified"

    },
    "qglviewer::Camera\0setPosition\0\0Vec\0"
    "pos\0setOrientation\0Quaternion\0q\0theta\0"
    "phi\0setUpVector\0up\0noMove\0setViewDirection\0"
    "direction\0lookAt\0target\0showEntireScene\0"
    "fitSphere\0center\0radius\0fitBoundingBox\0"
    "min\0max\0fitScreenRegion\0rectangle\0"
    "centerScene\0interpolateToZoomOnPixel\0"
    "pixel\0interpolateToFitScene\0interpolateTo\0"
    "Frame\0fr\0duration\0setType\0Type\0type\0"
    "setFieldOfView\0fov\0setHorizontalFieldOfView\0"
    "hfov\0setFOVToFitScene\0setAspectRatio\0"
    "aspect\0setScreenWidthAndHeight\0width\0"
    "height\0setZNearCoefficient\0coef\0"
    "setZClippingCoefficient\0setSceneRadius\0"
    "setSceneCenter\0setSceneCenterFromPixel\0"
    "setSceneBoundingBox\0setPivotPoint\0"
    "point\0setPivotPointFromPixel\0"
    "setRevolveAroundPoint\0"
    "setRevolveAroundPointFromPixel\0setFrame\0"
    "ManipulatedCameraFrame*\0mcf\0"
    "setKeyFrameInterpolator\0i\0"
    "KeyFrameInterpolator*\0kfi\0addKeyFrameToPath\0"
    "playPath\0deletePath\0resetPath\0"
    "drawAllPaths\0setFlySpeed\0speed\0"
    "setIODistance\0distance\0"
    "setPhysicalDistanceToScreen\0"
    "setPhysicalScreenWidth\0setFocusDistance\0"
    "initFromDOMElement\0QDomElement\0element\0"
    "onFrameModified"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_qglviewer__Camera[] = {

 // content:
      10,       // revision
       0,       // classname
       0,    0, // classinfo
      45,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,  284,    2, 0x0a,    1 /* Public */,
       5,    1,  287,    2, 0x0a,    3 /* Public */,
       5,    2,  290,    2, 0x0a,    5 /* Public */,
      10,    2,  295,    2, 0x0a,    8 /* Public */,
      10,    1,  300,    2, 0x2a,   11 /* Public | MethodCloned */,
      13,    1,  303,    2, 0x0a,   13 /* Public */,
      15,    1,  306,    2, 0x0a,   15 /* Public */,
      17,    0,  309,    2, 0x0a,   17 /* Public */,
      18,    2,  310,    2, 0x0a,   18 /* Public */,
      21,    2,  315,    2, 0x0a,   21 /* Public */,
      24,    1,  320,    2, 0x0a,   24 /* Public */,
      26,    0,  323,    2, 0x0a,   26 /* Public */,
      27,    1,  324,    2, 0x0a,   27 /* Public */,
      29,    0,  327,    2, 0x0a,   29 /* Public */,
      30,    2,  328,    2, 0x0a,   30 /* Public */,
      34,    1,  333,    2, 0x0a,   33 /* Public */,
      37,    1,  336,    2, 0x0a,   35 /* Public */,
      39,    1,  339,    2, 0x0a,   37 /* Public */,
      41,    0,  342,    2, 0x0a,   39 /* Public */,
      42,    1,  343,    2, 0x0a,   40 /* Public */,
      44,    2,  346,    2, 0x0a,   42 /* Public */,
      47,    1,  351,    2, 0x0a,   45 /* Public */,
      49,    1,  354,    2, 0x0a,   47 /* Public */,
      50,    1,  357,    2, 0x0a,   49 /* Public */,
      51,    1,  360,    2, 0x0a,   51 /* Public */,
      52,    1,  363,    2, 0x0a,   53 /* Public */,
      53,    2,  366,    2, 0x0a,   55 /* Public */,
      54,    1,  371,    2, 0x0a,   58 /* Public */,
      56,    1,  374,    2, 0x0a,   60 /* Public */,
      57,    1,  377,    2, 0x0a,   62 /* Public */,
      58,    1,  380,    2, 0x0a,   64 /* Public */,
      59,    1,  383,    2, 0x0a,   66 /* Public */,
      62,    2,  386,    2, 0x0a,   68 /* Public */,
      66,    1,  391,    2, 0x0a,   71 /* Public */,
      67,    1,  394,    2, 0x0a,   73 /* Public */,
      68,    1,  397,    2, 0x0a,   75 /* Public */,
      69,    1,  400,    2, 0x0a,   77 /* Public */,
      70,    0,  403,    2, 0x0a,   79 /* Public */,
      71,    1,  404,    2, 0x0a,   80 /* Public */,
      73,    1,  407,    2, 0x0a,   82 /* Public */,
      75,    1,  410,    2, 0x0a,   84 /* Public */,
      76,    1,  413,    2, 0x0a,   86 /* Public */,
      77,    1,  416,    2, 0x0a,   88 /* Public */,
      78,    1,  419,    2, 0x0a,   90 /* Public */,
      81,    0,  422,    2, 0x08,   92 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::QReal, QMetaType::QReal,    8,    9,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Bool,   11,   12,
    QMetaType::Void, 0x80000000 | 3,   11,
    QMetaType::Void, 0x80000000 | 3,   14,
    QMetaType::Void, 0x80000000 | 3,   16,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3, QMetaType::QReal,   19,   20,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,   22,   23,
    QMetaType::Void, QMetaType::QRect,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   28,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 31, QMetaType::QReal,   32,   33,
    QMetaType::Void, 0x80000000 | 35,   36,
    QMetaType::Void, QMetaType::QReal,   38,
    QMetaType::Void, QMetaType::QReal,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,   43,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   45,   46,
    QMetaType::Void, QMetaType::QReal,   48,
    QMetaType::Void, QMetaType::QReal,   48,
    QMetaType::Void, QMetaType::QReal,   20,
    QMetaType::Void, 0x80000000 | 3,   19,
    QMetaType::Bool, QMetaType::QPoint,   28,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,   22,   23,
    QMetaType::Void, 0x80000000 | 3,   55,
    QMetaType::Bool, QMetaType::QPoint,   28,
    QMetaType::Void, 0x80000000 | 3,   55,
    QMetaType::Bool, QMetaType::QPoint,   28,
    QMetaType::Void, 0x80000000 | 60,   61,
    QMetaType::Void, QMetaType::UInt, 0x80000000 | 64,   63,   65,
    QMetaType::Void, QMetaType::UInt,   63,
    QMetaType::Void, QMetaType::UInt,   63,
    QMetaType::Void, QMetaType::UInt,   63,
    QMetaType::Void, QMetaType::UInt,   63,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,   72,
    QMetaType::Void, QMetaType::QReal,   74,
    QMetaType::Void, QMetaType::QReal,   74,
    QMetaType::Void, QMetaType::QReal,   45,
    QMetaType::Void, QMetaType::QReal,   74,
    QMetaType::Void, 0x80000000 | 79,   80,
    QMetaType::Void,

       0        // eod
};

void qglviewer::Camera::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Camera *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->setPosition((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1]))); break;
        case 1: _t->setOrientation((*reinterpret_cast< std::add_pointer_t<Quaternion>>(_a[1]))); break;
        case 2: _t->setOrientation((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<qreal>>(_a[2]))); break;
        case 3: _t->setUpVector((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[2]))); break;
        case 4: _t->setUpVector((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1]))); break;
        case 5: _t->setViewDirection((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1]))); break;
        case 6: _t->lookAt((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1]))); break;
        case 7: _t->showEntireScene(); break;
        case 8: _t->fitSphere((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<qreal>>(_a[2]))); break;
        case 9: _t->fitBoundingBox((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Vec>>(_a[2]))); break;
        case 10: _t->fitScreenRegion((*reinterpret_cast< std::add_pointer_t<QRect>>(_a[1]))); break;
        case 11: _t->centerScene(); break;
        case 12: _t->interpolateToZoomOnPixel((*reinterpret_cast< std::add_pointer_t<QPoint>>(_a[1]))); break;
        case 13: _t->interpolateToFitScene(); break;
        case 14: _t->interpolateTo((*reinterpret_cast< std::add_pointer_t<Frame>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<qreal>>(_a[2]))); break;
        case 15: _t->setType((*reinterpret_cast< std::add_pointer_t<Type>>(_a[1]))); break;
        case 16: _t->setFieldOfView((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 17: _t->setHorizontalFieldOfView((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 18: _t->setFOVToFitScene(); break;
        case 19: _t->setAspectRatio((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 20: _t->setScreenWidthAndHeight((*reinterpret_cast< std::add_pointer_t<int>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<int>>(_a[2]))); break;
        case 21: _t->setZNearCoefficient((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 22: _t->setZClippingCoefficient((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 23: _t->setSceneRadius((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 24: _t->setSceneCenter((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1]))); break;
        case 25: { bool _r = _t->setSceneCenterFromPixel((*reinterpret_cast< std::add_pointer_t<QPoint>>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 26: _t->setSceneBoundingBox((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Vec>>(_a[2]))); break;
        case 27: _t->setPivotPoint((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1]))); break;
        case 28: { bool _r = _t->setPivotPointFromPixel((*reinterpret_cast< std::add_pointer_t<QPoint>>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 29: _t->setRevolveAroundPoint((*reinterpret_cast< std::add_pointer_t<Vec>>(_a[1]))); break;
        case 30: { bool _r = _t->setRevolveAroundPointFromPixel((*reinterpret_cast< std::add_pointer_t<QPoint>>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 31: _t->setFrame((*reinterpret_cast< std::add_pointer_t<ManipulatedCameraFrame*>>(_a[1]))); break;
        case 32: _t->setKeyFrameInterpolator((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<KeyFrameInterpolator*>>(_a[2]))); break;
        case 33: _t->addKeyFrameToPath((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1]))); break;
        case 34: _t->playPath((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1]))); break;
        case 35: _t->deletePath((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1]))); break;
        case 36: _t->resetPath((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1]))); break;
        case 37: _t->drawAllPaths(); break;
        case 38: _t->setFlySpeed((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 39: _t->setIODistance((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 40: _t->setPhysicalDistanceToScreen((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 41: _t->setPhysicalScreenWidth((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 42: _t->setFocusDistance((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 43: _t->initFromDOMElement((*reinterpret_cast< std::add_pointer_t<QDomElement>>(_a[1]))); break;
        case 44: _t->onFrameModified(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType(); break;
        case 32:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType(); break;
            case 1:
                *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType::fromType< KeyFrameInterpolator* >(); break;
            }
            break;
        }
    }
}

const QMetaObject qglviewer::Camera::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_qglviewer__Camera.offsetsAndSize,
    qt_meta_data_qglviewer__Camera,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_qglviewer__Camera_t
, QtPrivate::TypeAndForceComplete<Camera, std::true_type>
, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Quaternion &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QRect &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QPoint &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Frame &, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Type, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<const QPoint &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<const QPoint &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<const QPoint &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<ManipulatedCameraFrame * const, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<KeyFrameInterpolator * const, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QDomElement &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>


>,
    nullptr
} };


const QMetaObject *qglviewer::Camera::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *qglviewer::Camera::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_qglviewer__Camera.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int qglviewer::Camera::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 45)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 45;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 45)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 45;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
