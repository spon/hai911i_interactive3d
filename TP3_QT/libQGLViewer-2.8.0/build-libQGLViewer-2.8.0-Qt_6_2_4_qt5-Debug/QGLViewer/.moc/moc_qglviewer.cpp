/****************************************************************************
** Meta object code from reading C++ file 'qglviewer.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.2.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../QGLViewer/qglviewer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qglviewer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.2.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGLViewer_t {
    const uint offsetsAndSize[306];
    char stringdata0[2140];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_QGLViewer_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_QGLViewer_t qt_meta_stringdata_QGLViewer = {
    {
QT_MOC_LITERAL(0, 9), // "QGLViewer"
QT_MOC_LITERAL(10, 17), // "viewerInitialized"
QT_MOC_LITERAL(28, 0), // ""
QT_MOC_LITERAL(29, 10), // "drawNeeded"
QT_MOC_LITERAL(40, 12), // "drawFinished"
QT_MOC_LITERAL(53, 9), // "automatic"
QT_MOC_LITERAL(63, 13), // "animateNeeded"
QT_MOC_LITERAL(77, 12), // "helpRequired"
QT_MOC_LITERAL(90, 18), // "axisIsDrawnChanged"
QT_MOC_LITERAL(109, 5), // "drawn"
QT_MOC_LITERAL(115, 18), // "gridIsDrawnChanged"
QT_MOC_LITERAL(134, 21), // "FPSIsDisplayedChanged"
QT_MOC_LITERAL(156, 9), // "displayed"
QT_MOC_LITERAL(166, 20), // "textIsEnabledChanged"
QT_MOC_LITERAL(187, 7), // "enabled"
QT_MOC_LITERAL(195, 21), // "cameraIsEditedChanged"
QT_MOC_LITERAL(217, 6), // "edited"
QT_MOC_LITERAL(224, 13), // "stereoChanged"
QT_MOC_LITERAL(238, 2), // "on"
QT_MOC_LITERAL(241, 13), // "pointSelected"
QT_MOC_LITERAL(255, 18), // "const QMouseEvent*"
QT_MOC_LITERAL(274, 1), // "e"
QT_MOC_LITERAL(276, 19), // "mouseGrabberChanged"
QT_MOC_LITERAL(296, 24), // "qglviewer::MouseGrabber*"
QT_MOC_LITERAL(321, 12), // "mouseGrabber"
QT_MOC_LITERAL(334, 14), // "setAxisIsDrawn"
QT_MOC_LITERAL(349, 4), // "draw"
QT_MOC_LITERAL(354, 14), // "setGridIsDrawn"
QT_MOC_LITERAL(369, 17), // "setFPSIsDisplayed"
QT_MOC_LITERAL(387, 7), // "display"
QT_MOC_LITERAL(395, 16), // "setTextIsEnabled"
QT_MOC_LITERAL(412, 6), // "enable"
QT_MOC_LITERAL(419, 17), // "setCameraIsEdited"
QT_MOC_LITERAL(437, 4), // "edit"
QT_MOC_LITERAL(442, 17), // "toggleAxisIsDrawn"
QT_MOC_LITERAL(460, 17), // "toggleGridIsDrawn"
QT_MOC_LITERAL(478, 20), // "toggleFPSIsDisplayed"
QT_MOC_LITERAL(499, 19), // "toggleTextIsEnabled"
QT_MOC_LITERAL(519, 20), // "toggleCameraIsEdited"
QT_MOC_LITERAL(540, 18), // "setBackgroundColor"
QT_MOC_LITERAL(559, 5), // "color"
QT_MOC_LITERAL(565, 18), // "setForegroundColor"
QT_MOC_LITERAL(584, 14), // "setSceneRadius"
QT_MOC_LITERAL(599, 6), // "radius"
QT_MOC_LITERAL(606, 14), // "setSceneCenter"
QT_MOC_LITERAL(621, 14), // "qglviewer::Vec"
QT_MOC_LITERAL(636, 6), // "center"
QT_MOC_LITERAL(643, 19), // "setSceneBoundingBox"
QT_MOC_LITERAL(663, 3), // "min"
QT_MOC_LITERAL(667, 3), // "max"
QT_MOC_LITERAL(671, 15), // "showEntireScene"
QT_MOC_LITERAL(687, 9), // "setCamera"
QT_MOC_LITERAL(697, 18), // "qglviewer::Camera*"
QT_MOC_LITERAL(716, 6), // "camera"
QT_MOC_LITERAL(723, 19), // "setManipulatedFrame"
QT_MOC_LITERAL(743, 28), // "qglviewer::ManipulatedFrame*"
QT_MOC_LITERAL(772, 5), // "frame"
QT_MOC_LITERAL(778, 15), // "setMouseGrabber"
QT_MOC_LITERAL(794, 13), // "setFullScreen"
QT_MOC_LITERAL(808, 10), // "fullScreen"
QT_MOC_LITERAL(819, 16), // "setStereoDisplay"
QT_MOC_LITERAL(836, 6), // "stereo"
QT_MOC_LITERAL(843, 16), // "toggleFullScreen"
QT_MOC_LITERAL(860, 19), // "toggleStereoDisplay"
QT_MOC_LITERAL(880, 16), // "toggleCameraMode"
QT_MOC_LITERAL(897, 12), // "saveSnapshot"
QT_MOC_LITERAL(910, 9), // "overwrite"
QT_MOC_LITERAL(920, 8), // "fileName"
QT_MOC_LITERAL(929, 19), // "setSnapshotFileName"
QT_MOC_LITERAL(949, 4), // "name"
QT_MOC_LITERAL(954, 17), // "setSnapshotFormat"
QT_MOC_LITERAL(972, 6), // "format"
QT_MOC_LITERAL(979, 18), // "setSnapshotCounter"
QT_MOC_LITERAL(998, 7), // "counter"
QT_MOC_LITERAL(1006, 18), // "setSnapshotQuality"
QT_MOC_LITERAL(1025, 7), // "quality"
QT_MOC_LITERAL(1033, 24), // "openSnapshotFormatDialog"
QT_MOC_LITERAL(1058, 19), // "snapshotToClipboard"
QT_MOC_LITERAL(1078, 19), // "copyBufferToTexture"
QT_MOC_LITERAL(1098, 5), // "GLint"
QT_MOC_LITERAL(1104, 14), // "internalFormat"
QT_MOC_LITERAL(1119, 6), // "GLenum"
QT_MOC_LITERAL(1126, 18), // "setAnimationPeriod"
QT_MOC_LITERAL(1145, 6), // "period"
QT_MOC_LITERAL(1152, 14), // "startAnimation"
QT_MOC_LITERAL(1167, 13), // "stopAnimation"
QT_MOC_LITERAL(1181, 7), // "animate"
QT_MOC_LITERAL(1189, 15), // "toggleAnimation"
QT_MOC_LITERAL(1205, 4), // "help"
QT_MOC_LITERAL(1210, 14), // "aboutQGLViewer"
QT_MOC_LITERAL(1225, 6), // "select"
QT_MOC_LITERAL(1232, 5), // "event"
QT_MOC_LITERAL(1238, 5), // "point"
QT_MOC_LITERAL(1244, 19), // "setSelectBufferSize"
QT_MOC_LITERAL(1264, 4), // "size"
QT_MOC_LITERAL(1269, 20), // "setSelectRegionWidth"
QT_MOC_LITERAL(1290, 5), // "width"
QT_MOC_LITERAL(1296, 21), // "setSelectRegionHeight"
QT_MOC_LITERAL(1318, 6), // "height"
QT_MOC_LITERAL(1325, 15), // "setSelectedName"
QT_MOC_LITERAL(1341, 2), // "id"
QT_MOC_LITERAL(1344, 11), // "setShortcut"
QT_MOC_LITERAL(1356, 14), // "KeyboardAction"
QT_MOC_LITERAL(1371, 6), // "action"
QT_MOC_LITERAL(1378, 3), // "key"
QT_MOC_LITERAL(1382, 22), // "setKeyboardAccelerator"
QT_MOC_LITERAL(1405, 17), // "setKeyDescription"
QT_MOC_LITERAL(1423, 11), // "description"
QT_MOC_LITERAL(1435, 14), // "clearShortcuts"
QT_MOC_LITERAL(1450, 14), // "setKeyFrameKey"
QT_MOC_LITERAL(1465, 5), // "index"
QT_MOC_LITERAL(1471, 27), // "setPlayKeyFramePathStateKey"
QT_MOC_LITERAL(1499, 11), // "buttonState"
QT_MOC_LITERAL(1511, 19), // "setPlayPathStateKey"
QT_MOC_LITERAL(1531, 22), // "setAddKeyFrameStateKey"
QT_MOC_LITERAL(1554, 10), // "setPathKey"
QT_MOC_LITERAL(1565, 28), // "setPlayPathKeyboardModifiers"
QT_MOC_LITERAL(1594, 21), // "Qt::KeyboardModifiers"
QT_MOC_LITERAL(1616, 9), // "modifiers"
QT_MOC_LITERAL(1626, 31), // "setAddKeyFrameKeyboardModifiers"
QT_MOC_LITERAL(1658, 15), // "setMouseBinding"
QT_MOC_LITERAL(1674, 5), // "state"
QT_MOC_LITERAL(1680, 12), // "MouseHandler"
QT_MOC_LITERAL(1693, 7), // "handler"
QT_MOC_LITERAL(1701, 11), // "MouseAction"
QT_MOC_LITERAL(1713, 14), // "withConstraint"
QT_MOC_LITERAL(1728, 11), // "ClickAction"
QT_MOC_LITERAL(1740, 11), // "doubleClick"
QT_MOC_LITERAL(1752, 16), // "Qt::MouseButtons"
QT_MOC_LITERAL(1769, 13), // "buttonsBefore"
QT_MOC_LITERAL(1783, 26), // "setMouseBindingDescription"
QT_MOC_LITERAL(1810, 15), // "Qt::MouseButton"
QT_MOC_LITERAL(1826, 7), // "buttons"
QT_MOC_LITERAL(1834, 6), // "button"
QT_MOC_LITERAL(1841, 15), // "setWheelBinding"
QT_MOC_LITERAL(1857, 7), // "Qt::Key"
QT_MOC_LITERAL(1865, 18), // "clearMouseBindings"
QT_MOC_LITERAL(1884, 11), // "wheelAction"
QT_MOC_LITERAL(1896, 12), // "wheelHandler"
QT_MOC_LITERAL(1909, 27), // "setHandlerKeyboardModifiers"
QT_MOC_LITERAL(1937, 18), // "setHandlerStateKey"
QT_MOC_LITERAL(1956, 16), // "setMouseStateKey"
QT_MOC_LITERAL(1973, 18), // "initFromDOMElement"
QT_MOC_LITERAL(1992, 11), // "QDomElement"
QT_MOC_LITERAL(2004, 7), // "element"
QT_MOC_LITERAL(2012, 15), // "saveStateToFile"
QT_MOC_LITERAL(2028, 20), // "restoreStateFromFile"
QT_MOC_LITERAL(2049, 16), // "setStateFileName"
QT_MOC_LITERAL(2066, 10), // "saveToFile"
QT_MOC_LITERAL(2077, 15), // "restoreFromFile"
QT_MOC_LITERAL(2093, 16), // "resetVisualHints"
QT_MOC_LITERAL(2110, 17), // "delayedFullScreen"
QT_MOC_LITERAL(2128, 11) // "hideMessage"

    },
    "QGLViewer\0viewerInitialized\0\0drawNeeded\0"
    "drawFinished\0automatic\0animateNeeded\0"
    "helpRequired\0axisIsDrawnChanged\0drawn\0"
    "gridIsDrawnChanged\0FPSIsDisplayedChanged\0"
    "displayed\0textIsEnabledChanged\0enabled\0"
    "cameraIsEditedChanged\0edited\0stereoChanged\0"
    "on\0pointSelected\0const QMouseEvent*\0"
    "e\0mouseGrabberChanged\0qglviewer::MouseGrabber*\0"
    "mouseGrabber\0setAxisIsDrawn\0draw\0"
    "setGridIsDrawn\0setFPSIsDisplayed\0"
    "display\0setTextIsEnabled\0enable\0"
    "setCameraIsEdited\0edit\0toggleAxisIsDrawn\0"
    "toggleGridIsDrawn\0toggleFPSIsDisplayed\0"
    "toggleTextIsEnabled\0toggleCameraIsEdited\0"
    "setBackgroundColor\0color\0setForegroundColor\0"
    "setSceneRadius\0radius\0setSceneCenter\0"
    "qglviewer::Vec\0center\0setSceneBoundingBox\0"
    "min\0max\0showEntireScene\0setCamera\0"
    "qglviewer::Camera*\0camera\0setManipulatedFrame\0"
    "qglviewer::ManipulatedFrame*\0frame\0"
    "setMouseGrabber\0setFullScreen\0fullScreen\0"
    "setStereoDisplay\0stereo\0toggleFullScreen\0"
    "toggleStereoDisplay\0toggleCameraMode\0"
    "saveSnapshot\0overwrite\0fileName\0"
    "setSnapshotFileName\0name\0setSnapshotFormat\0"
    "format\0setSnapshotCounter\0counter\0"
    "setSnapshotQuality\0quality\0"
    "openSnapshotFormatDialog\0snapshotToClipboard\0"
    "copyBufferToTexture\0GLint\0internalFormat\0"
    "GLenum\0setAnimationPeriod\0period\0"
    "startAnimation\0stopAnimation\0animate\0"
    "toggleAnimation\0help\0aboutQGLViewer\0"
    "select\0event\0point\0setSelectBufferSize\0"
    "size\0setSelectRegionWidth\0width\0"
    "setSelectRegionHeight\0height\0"
    "setSelectedName\0id\0setShortcut\0"
    "KeyboardAction\0action\0key\0"
    "setKeyboardAccelerator\0setKeyDescription\0"
    "description\0clearShortcuts\0setKeyFrameKey\0"
    "index\0setPlayKeyFramePathStateKey\0"
    "buttonState\0setPlayPathStateKey\0"
    "setAddKeyFrameStateKey\0setPathKey\0"
    "setPlayPathKeyboardModifiers\0"
    "Qt::KeyboardModifiers\0modifiers\0"
    "setAddKeyFrameKeyboardModifiers\0"
    "setMouseBinding\0state\0MouseHandler\0"
    "handler\0MouseAction\0withConstraint\0"
    "ClickAction\0doubleClick\0Qt::MouseButtons\0"
    "buttonsBefore\0setMouseBindingDescription\0"
    "Qt::MouseButton\0buttons\0button\0"
    "setWheelBinding\0Qt::Key\0clearMouseBindings\0"
    "wheelAction\0wheelHandler\0"
    "setHandlerKeyboardModifiers\0"
    "setHandlerStateKey\0setMouseStateKey\0"
    "initFromDOMElement\0QDomElement\0element\0"
    "saveStateToFile\0restoreStateFromFile\0"
    "setStateFileName\0saveToFile\0restoreFromFile\0"
    "resetVisualHints\0delayedFullScreen\0"
    "hideMessage"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGLViewer[] = {

 // content:
      10,       // revision
       0,       // classname
       0,    0, // classinfo
     127,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,  776,    2, 0x06,    1 /* Public */,
       3,    0,  777,    2, 0x06,    2 /* Public */,
       4,    1,  778,    2, 0x06,    3 /* Public */,
       6,    0,  781,    2, 0x06,    5 /* Public */,
       7,    0,  782,    2, 0x06,    6 /* Public */,
       8,    1,  783,    2, 0x06,    7 /* Public */,
      10,    1,  786,    2, 0x06,    9 /* Public */,
      11,    1,  789,    2, 0x06,   11 /* Public */,
      13,    1,  792,    2, 0x06,   13 /* Public */,
      15,    1,  795,    2, 0x06,   15 /* Public */,
      17,    1,  798,    2, 0x06,   17 /* Public */,
      19,    1,  801,    2, 0x06,   19 /* Public */,
      22,    1,  804,    2, 0x06,   21 /* Public */,

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
      25,    1,  807,    2, 0x0a,   23 /* Public */,
      25,    0,  810,    2, 0x2a,   25 /* Public | MethodCloned */,
      27,    1,  811,    2, 0x0a,   26 /* Public */,
      27,    0,  814,    2, 0x2a,   28 /* Public | MethodCloned */,
      28,    1,  815,    2, 0x0a,   29 /* Public */,
      28,    0,  818,    2, 0x2a,   31 /* Public | MethodCloned */,
      30,    1,  819,    2, 0x0a,   32 /* Public */,
      30,    0,  822,    2, 0x2a,   34 /* Public | MethodCloned */,
      32,    1,  823,    2, 0x0a,   35 /* Public */,
      32,    0,  826,    2, 0x2a,   37 /* Public | MethodCloned */,
      34,    0,  827,    2, 0x0a,   38 /* Public */,
      35,    0,  828,    2, 0x0a,   39 /* Public */,
      36,    0,  829,    2, 0x0a,   40 /* Public */,
      37,    0,  830,    2, 0x0a,   41 /* Public */,
      38,    0,  831,    2, 0x0a,   42 /* Public */,
      39,    1,  832,    2, 0x0a,   43 /* Public */,
      41,    1,  835,    2, 0x0a,   45 /* Public */,
      42,    1,  838,    2, 0x0a,   47 /* Public */,
      44,    1,  841,    2, 0x0a,   49 /* Public */,
      47,    2,  844,    2, 0x0a,   51 /* Public */,
      50,    0,  849,    2, 0x0a,   54 /* Public */,
      51,    1,  850,    2, 0x0a,   55 /* Public */,
      54,    1,  853,    2, 0x0a,   57 /* Public */,
      57,    1,  856,    2, 0x0a,   59 /* Public */,
      58,    1,  859,    2, 0x0a,   61 /* Public */,
      58,    0,  862,    2, 0x2a,   63 /* Public | MethodCloned */,
      60,    1,  863,    2, 0x0a,   64 /* Public */,
      60,    0,  866,    2, 0x2a,   66 /* Public | MethodCloned */,
      62,    0,  867,    2, 0x0a,   67 /* Public */,
      63,    0,  868,    2, 0x0a,   68 /* Public */,
      64,    0,  869,    2, 0x0a,   69 /* Public */,
      65,    2,  870,    2, 0x0a,   70 /* Public */,
      65,    1,  875,    2, 0x2a,   73 /* Public | MethodCloned */,
      65,    0,  878,    2, 0x2a,   75 /* Public | MethodCloned */,
      65,    2,  879,    2, 0x0a,   76 /* Public */,
      65,    1,  884,    2, 0x2a,   79 /* Public | MethodCloned */,
      68,    1,  887,    2, 0x0a,   81 /* Public */,
      70,    1,  890,    2, 0x0a,   83 /* Public */,
      72,    1,  893,    2, 0x0a,   85 /* Public */,
      74,    1,  896,    2, 0x0a,   87 /* Public */,
      76,    0,  899,    2, 0x0a,   89 /* Public */,
      77,    0,  900,    2, 0x0a,   90 /* Public */,
      78,    2,  901,    2, 0x0a,   91 /* Public */,
      78,    1,  906,    2, 0x2a,   94 /* Public | MethodCloned */,
      82,    1,  909,    2, 0x0a,   96 /* Public */,
      84,    0,  912,    2, 0x0a,   98 /* Public */,
      85,    0,  913,    2, 0x0a,   99 /* Public */,
      86,    0,  914,    2, 0x0a,  100 /* Public */,
      87,    0,  915,    2, 0x0a,  101 /* Public */,
      88,    0,  916,    2, 0x0a,  102 /* Public */,
      89,    0,  917,    2, 0x0a,  103 /* Public */,
      90,    1,  918,    2, 0x0a,  104 /* Public */,
      90,    1,  921,    2, 0x0a,  106 /* Public */,
      93,    1,  924,    2, 0x0a,  108 /* Public */,
      95,    1,  927,    2, 0x0a,  110 /* Public */,
      97,    1,  930,    2, 0x0a,  112 /* Public */,
      99,    1,  933,    2, 0x0a,  114 /* Public */,
     101,    2,  936,    2, 0x0a,  116 /* Public */,
     105,    2,  941,    2, 0x0a,  119 /* Public */,
     106,    2,  946,    2, 0x0a,  122 /* Public */,
     108,    0,  951,    2, 0x0a,  125 /* Public */,
     109,    2,  952,    2, 0x0a,  126 /* Public */,
     111,    1,  957,    2, 0x0a,  129 /* Public */,
     113,    1,  960,    2, 0x0a,  131 /* Public */,
     114,    1,  963,    2, 0x0a,  133 /* Public */,
     115,    2,  966,    2, 0x0a,  135 /* Public */,
     115,    1,  971,    2, 0x2a,  138 /* Public | MethodCloned */,
     116,    1,  974,    2, 0x0a,  140 /* Public */,
     119,    1,  977,    2, 0x0a,  142 /* Public */,
     120,    4,  980,    2, 0x0a,  144 /* Public */,
     120,    3,  989,    2, 0x2a,  149 /* Public | MethodCloned */,
     120,    4,  996,    2, 0x0a,  153 /* Public */,
     120,    3, 1005,    2, 0x2a,  158 /* Public | MethodCloned */,
     120,    2, 1012,    2, 0x2a,  162 /* Public | MethodCloned */,
     130,    4, 1017,    2, 0x0a,  165 /* Public */,
     130,    3, 1026,    2, 0x2a,  170 /* Public | MethodCloned */,
     130,    2, 1033,    2, 0x2a,  174 /* Public | MethodCloned */,
     120,    5, 1038,    2, 0x0a,  177 /* Public */,
     120,    4, 1049,    2, 0x2a,  183 /* Public | MethodCloned */,
     120,    5, 1058,    2, 0x0a,  188 /* Public */,
     120,    4, 1069,    2, 0x2a,  194 /* Public | MethodCloned */,
     120,    3, 1078,    2, 0x2a,  199 /* Public | MethodCloned */,
     134,    4, 1085,    2, 0x0a,  203 /* Public */,
     134,    3, 1094,    2, 0x2a,  208 /* Public | MethodCloned */,
     130,    5, 1101,    2, 0x0a,  212 /* Public */,
     130,    4, 1112,    2, 0x2a,  218 /* Public | MethodCloned */,
     130,    3, 1121,    2, 0x2a,  223 /* Public | MethodCloned */,
     120,    6, 1128,    2, 0x0a,  227 /* Public */,
     120,    5, 1141,    2, 0x2a,  234 /* Public | MethodCloned */,
     120,    6, 1152,    2, 0x0a,  240 /* Public */,
     120,    5, 1165,    2, 0x2a,  247 /* Public | MethodCloned */,
     120,    4, 1176,    2, 0x2a,  253 /* Public | MethodCloned */,
     134,    5, 1185,    2, 0x0a,  258 /* Public */,
     134,    4, 1196,    2, 0x2a,  264 /* Public | MethodCloned */,
     130,    6, 1205,    2, 0x0a,  269 /* Public */,
     130,    5, 1218,    2, 0x2a,  276 /* Public | MethodCloned */,
     130,    4, 1229,    2, 0x2a,  282 /* Public | MethodCloned */,
     136,    0, 1238,    2, 0x0a,  287 /* Public */,
     137,    1, 1239,    2, 0x10a,  288 /* Public | MethodIsConst  */,
     138,    1, 1242,    2, 0x10a,  290 /* Public | MethodIsConst  */,
     139,    2, 1245,    2, 0x0a,  292 /* Public */,
     140,    2, 1250,    2, 0x0a,  295 /* Public */,
     141,    2, 1255,    2, 0x0a,  298 /* Public */,
     142,    1, 1260,    2, 0x0a,  301 /* Public */,
     145,    0, 1263,    2, 0x0a,  303 /* Public */,
     146,    0, 1264,    2, 0x0a,  304 /* Public */,
     147,    1, 1265,    2, 0x0a,  305 /* Public */,
     148,    1, 1268,    2, 0x0a,  307 /* Public */,
     148,    0, 1271,    2, 0x2a,  309 /* Public | MethodCloned */,
     149,    1, 1272,    2, 0x0a,  310 /* Public */,
     149,    0, 1275,    2, 0x2a,  312 /* Public | MethodCloned */,
     150,    0, 1276,    2, 0x0a,  313 /* Public */,
     151,    0, 1277,    2, 0x08,  314 /* Private */,
     152,    0, 1278,    2, 0x08,  315 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, 0x80000000 | 23,   24,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,   26,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   26,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   29,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   31,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   33,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QColor,   40,
    QMetaType::Void, QMetaType::QColor,   40,
    QMetaType::Void, QMetaType::QReal,   43,
    QMetaType::Void, 0x80000000 | 45,   46,
    QMetaType::Void, 0x80000000 | 45, 0x80000000 | 45,   48,   49,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 52,   53,
    QMetaType::Void, 0x80000000 | 55,   56,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void, QMetaType::Bool,   59,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   61,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool, QMetaType::Bool,    5,   66,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,   67,   66,
    QMetaType::Void, QMetaType::QString,   67,
    QMetaType::Void, QMetaType::QString,   69,
    QMetaType::Void, QMetaType::QString,   71,
    QMetaType::Void, QMetaType::Int,   73,
    QMetaType::Void, QMetaType::Int,   75,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 79, 0x80000000 | 81,   80,   71,
    QMetaType::Void, 0x80000000 | 79,   80,
    QMetaType::Void, QMetaType::Int,   83,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 20,   91,
    QMetaType::Void, QMetaType::QPoint,   92,
    QMetaType::Void, QMetaType::Int,   94,
    QMetaType::Void, QMetaType::Int,   96,
    QMetaType::Void, QMetaType::Int,   98,
    QMetaType::Void, QMetaType::Int,  100,
    QMetaType::Void, 0x80000000 | 102, QMetaType::UInt,  103,  104,
    QMetaType::Void, 0x80000000 | 102, QMetaType::UInt,  103,  104,
    QMetaType::Void, QMetaType::UInt, QMetaType::QString,  104,  107,
    QMetaType::Void,
    QMetaType::Void, QMetaType::UInt, QMetaType::Int,  110,  104,
    QMetaType::Void, QMetaType::UInt,  112,
    QMetaType::Void, QMetaType::UInt,  112,
    QMetaType::Void, QMetaType::UInt,  112,
    QMetaType::Void, QMetaType::Int, QMetaType::UInt,  104,  110,
    QMetaType::Void, QMetaType::Int,  104,
    QMetaType::Void, 0x80000000 | 117,  118,
    QMetaType::Void, 0x80000000 | 117,  118,
    QMetaType::Void, QMetaType::UInt, 0x80000000 | 122, 0x80000000 | 124, QMetaType::Bool,  121,  123,  103,  125,
    QMetaType::Void, QMetaType::UInt, 0x80000000 | 122, 0x80000000 | 124,  121,  123,  103,
    QMetaType::Void, QMetaType::UInt, 0x80000000 | 126, QMetaType::Bool, 0x80000000 | 128,  121,  103,  127,  129,
    QMetaType::Void, QMetaType::UInt, 0x80000000 | 126, QMetaType::Bool,  121,  103,  127,
    QMetaType::Void, QMetaType::UInt, 0x80000000 | 126,  121,  103,
    QMetaType::Void, QMetaType::UInt, QMetaType::QString, QMetaType::Bool, 0x80000000 | 128,  121,  107,  127,  129,
    QMetaType::Void, QMetaType::UInt, QMetaType::QString, QMetaType::Bool,  121,  107,  127,
    QMetaType::Void, QMetaType::UInt, QMetaType::QString,  121,  107,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 122, 0x80000000 | 124, QMetaType::Bool,  118,  132,  123,  103,  125,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 122, 0x80000000 | 124,  118,  132,  123,  103,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 126, QMetaType::Bool, 0x80000000 | 128,  118,  133,  103,  127,  129,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 126, QMetaType::Bool,  118,  133,  103,  127,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 126,  118,  133,  103,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 122, 0x80000000 | 124, QMetaType::Bool,  118,  123,  103,  125,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 122, 0x80000000 | 124,  118,  123,  103,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 131, QMetaType::QString, QMetaType::Bool, 0x80000000 | 128,  118,  133,  107,  127,  129,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 131, QMetaType::QString, QMetaType::Bool,  118,  133,  107,  127,
    QMetaType::Void, 0x80000000 | 117, 0x80000000 | 131, QMetaType::QString,  118,  133,  107,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 122, 0x80000000 | 124, QMetaType::Bool,  104,  118,  132,  123,  103,  125,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 122, 0x80000000 | 124,  104,  118,  132,  123,  103,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 126, QMetaType::Bool, 0x80000000 | 128,  104,  118,  133,  103,  127,  129,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 126, QMetaType::Bool,  104,  118,  133,  103,  127,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 131, 0x80000000 | 126,  104,  118,  133,  103,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 122, 0x80000000 | 124, QMetaType::Bool,  104,  118,  123,  103,  125,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 122, 0x80000000 | 124,  104,  118,  123,  103,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 131, QMetaType::QString, QMetaType::Bool, 0x80000000 | 128,  104,  118,  133,  107,  127,  129,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 131, QMetaType::QString, QMetaType::Bool,  104,  118,  133,  107,  127,
    QMetaType::Void, 0x80000000 | 135, 0x80000000 | 117, 0x80000000 | 131, QMetaType::QString,  104,  118,  133,  107,
    QMetaType::Void,
    0x80000000 | 124, 0x80000000 | 117,  118,
    QMetaType::Int, 0x80000000 | 117,  118,
    QMetaType::Void, 0x80000000 | 122, 0x80000000 | 117,  123,  118,
    QMetaType::Void, 0x80000000 | 122, QMetaType::UInt,  123,  112,
    QMetaType::Void, 0x80000000 | 122, QMetaType::UInt,  123,  112,
    QMetaType::Void, 0x80000000 | 143,  144,
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::Void, QMetaType::QString,   69,
    QMetaType::Void, QMetaType::QString,   67,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::QString,   67,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void QGLViewer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QGLViewer *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->viewerInitialized(); break;
        case 1: _t->drawNeeded(); break;
        case 2: _t->drawFinished((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 3: _t->animateNeeded(); break;
        case 4: _t->helpRequired(); break;
        case 5: _t->axisIsDrawnChanged((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 6: _t->gridIsDrawnChanged((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 7: _t->FPSIsDisplayedChanged((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 8: _t->textIsEnabledChanged((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 9: _t->cameraIsEditedChanged((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 10: _t->stereoChanged((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 11: _t->pointSelected((*reinterpret_cast< std::add_pointer_t<const QMouseEvent*>>(_a[1]))); break;
        case 12: _t->mouseGrabberChanged((*reinterpret_cast< std::add_pointer_t<qglviewer::MouseGrabber*>>(_a[1]))); break;
        case 13: _t->setAxisIsDrawn((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 14: _t->setAxisIsDrawn(); break;
        case 15: _t->setGridIsDrawn((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 16: _t->setGridIsDrawn(); break;
        case 17: _t->setFPSIsDisplayed((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 18: _t->setFPSIsDisplayed(); break;
        case 19: _t->setTextIsEnabled((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 20: _t->setTextIsEnabled(); break;
        case 21: _t->setCameraIsEdited((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 22: _t->setCameraIsEdited(); break;
        case 23: _t->toggleAxisIsDrawn(); break;
        case 24: _t->toggleGridIsDrawn(); break;
        case 25: _t->toggleFPSIsDisplayed(); break;
        case 26: _t->toggleTextIsEnabled(); break;
        case 27: _t->toggleCameraIsEdited(); break;
        case 28: _t->setBackgroundColor((*reinterpret_cast< std::add_pointer_t<QColor>>(_a[1]))); break;
        case 29: _t->setForegroundColor((*reinterpret_cast< std::add_pointer_t<QColor>>(_a[1]))); break;
        case 30: _t->setSceneRadius((*reinterpret_cast< std::add_pointer_t<qreal>>(_a[1]))); break;
        case 31: _t->setSceneCenter((*reinterpret_cast< std::add_pointer_t<qglviewer::Vec>>(_a[1]))); break;
        case 32: _t->setSceneBoundingBox((*reinterpret_cast< std::add_pointer_t<qglviewer::Vec>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<qglviewer::Vec>>(_a[2]))); break;
        case 33: _t->showEntireScene(); break;
        case 34: _t->setCamera((*reinterpret_cast< std::add_pointer_t<qglviewer::Camera*>>(_a[1]))); break;
        case 35: _t->setManipulatedFrame((*reinterpret_cast< std::add_pointer_t<qglviewer::ManipulatedFrame*>>(_a[1]))); break;
        case 36: _t->setMouseGrabber((*reinterpret_cast< std::add_pointer_t<qglviewer::MouseGrabber*>>(_a[1]))); break;
        case 37: _t->setFullScreen((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 38: _t->setFullScreen(); break;
        case 39: _t->setStereoDisplay((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 40: _t->setStereoDisplay(); break;
        case 41: _t->toggleFullScreen(); break;
        case 42: _t->toggleStereoDisplay(); break;
        case 43: _t->toggleCameraMode(); break;
        case 44: _t->saveSnapshot((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[2]))); break;
        case 45: _t->saveSnapshot((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 46: _t->saveSnapshot(); break;
        case 47: _t->saveSnapshot((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[2]))); break;
        case 48: _t->saveSnapshot((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1]))); break;
        case 49: _t->setSnapshotFileName((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1]))); break;
        case 50: _t->setSnapshotFormat((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1]))); break;
        case 51: _t->setSnapshotCounter((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 52: _t->setSnapshotQuality((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 53: { bool _r = _t->openSnapshotFormatDialog();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 54: _t->snapshotToClipboard(); break;
        case 55: _t->copyBufferToTexture((*reinterpret_cast< std::add_pointer_t<GLint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<GLenum>>(_a[2]))); break;
        case 56: _t->copyBufferToTexture((*reinterpret_cast< std::add_pointer_t<GLint>>(_a[1]))); break;
        case 57: _t->setAnimationPeriod((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 58: _t->startAnimation(); break;
        case 59: _t->stopAnimation(); break;
        case 60: _t->animate(); break;
        case 61: _t->toggleAnimation(); break;
        case 62: _t->help(); break;
        case 63: _t->aboutQGLViewer(); break;
        case 64: _t->select((*reinterpret_cast< std::add_pointer_t<const QMouseEvent*>>(_a[1]))); break;
        case 65: _t->select((*reinterpret_cast< std::add_pointer_t<QPoint>>(_a[1]))); break;
        case 66: _t->setSelectBufferSize((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 67: _t->setSelectRegionWidth((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 68: _t->setSelectRegionHeight((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 69: _t->setSelectedName((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 70: _t->setShortcut((*reinterpret_cast< std::add_pointer_t<KeyboardAction>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<uint>>(_a[2]))); break;
        case 71: _t->setKeyboardAccelerator((*reinterpret_cast< std::add_pointer_t<KeyboardAction>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<uint>>(_a[2]))); break;
        case 72: _t->setKeyDescription((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[2]))); break;
        case 73: _t->clearShortcuts(); break;
        case 74: _t->setKeyFrameKey((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<int>>(_a[2]))); break;
        case 75: _t->setPlayKeyFramePathStateKey((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1]))); break;
        case 76: _t->setPlayPathStateKey((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1]))); break;
        case 77: _t->setAddKeyFrameStateKey((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1]))); break;
        case 78: _t->setPathKey((*reinterpret_cast< std::add_pointer_t<int>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<uint>>(_a[2]))); break;
        case 79: _t->setPathKey((*reinterpret_cast< std::add_pointer_t<int>>(_a[1]))); break;
        case 80: _t->setPlayPathKeyboardModifiers((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1]))); break;
        case 81: _t->setAddKeyFrameKeyboardModifiers((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1]))); break;
        case 82: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[4]))); break;
        case 83: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[3]))); break;
        case 84: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButtons>>(_a[4]))); break;
        case 85: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[3]))); break;
        case 86: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[2]))); break;
        case 87: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButtons>>(_a[4]))); break;
        case 88: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[3]))); break;
        case 89: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<uint>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[2]))); break;
        case 90: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[5]))); break;
        case 91: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[4]))); break;
        case 92: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButtons>>(_a[5]))); break;
        case 93: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[4]))); break;
        case 94: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[3]))); break;
        case 95: _t->setWheelBinding((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[4]))); break;
        case 96: _t->setWheelBinding((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[3]))); break;
        case 97: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButtons>>(_a[5]))); break;
        case 98: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[4]))); break;
        case 99: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[3]))); break;
        case 100: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[5])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[6]))); break;
        case 101: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[5]))); break;
        case 102: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[5])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButtons>>(_a[6]))); break;
        case 103: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[5]))); break;
        case 104: _t->setMouseBinding((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<ClickAction>>(_a[4]))); break;
        case 105: _t->setWheelBinding((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[5]))); break;
        case 106: _t->setWheelBinding((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<MouseAction>>(_a[4]))); break;
        case 107: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[5])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButtons>>(_a[6]))); break;
        case 108: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[4])),(*reinterpret_cast< std::add_pointer_t<bool>>(_a[5]))); break;
        case 109: _t->setMouseBindingDescription((*reinterpret_cast< std::add_pointer_t<Qt::Key>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2])),(*reinterpret_cast< std::add_pointer_t<Qt::MouseButton>>(_a[3])),(*reinterpret_cast< std::add_pointer_t<QString>>(_a[4]))); break;
        case 110: _t->clearMouseBindings(); break;
        case 111: { MouseAction _r = _t->wheelAction((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])));
            if (_a[0]) *reinterpret_cast< MouseAction*>(_a[0]) = std::move(_r); }  break;
        case 112: { int _r = _t->wheelHandler((*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 113: _t->setHandlerKeyboardModifiers((*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<Qt::KeyboardModifiers>>(_a[2]))); break;
        case 114: _t->setHandlerStateKey((*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<uint>>(_a[2]))); break;
        case 115: _t->setMouseStateKey((*reinterpret_cast< std::add_pointer_t<MouseHandler>>(_a[1])),(*reinterpret_cast< std::add_pointer_t<uint>>(_a[2]))); break;
        case 116: _t->initFromDOMElement((*reinterpret_cast< std::add_pointer_t<QDomElement>>(_a[1]))); break;
        case 117: _t->saveStateToFile(); break;
        case 118: { bool _r = _t->restoreStateFromFile();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 119: _t->setStateFileName((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1]))); break;
        case 120: _t->saveToFile((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1]))); break;
        case 121: _t->saveToFile(); break;
        case 122: { bool _r = _t->restoreFromFile((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 123: { bool _r = _t->restoreFromFile();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 124: _t->resetVisualHints(); break;
        case 125: _t->delayedFullScreen(); break;
        case 126: _t->hideMessage(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType(); break;
        case 34:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType(); break;
            case 0:
                *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType::fromType< qglviewer::Camera* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QGLViewer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::viewerInitialized)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::drawNeeded)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::drawFinished)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::animateNeeded)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::helpRequired)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::axisIsDrawnChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::gridIsDrawnChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::FPSIsDisplayedChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::textIsEnabledChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::cameraIsEditedChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::stereoChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(const QMouseEvent * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::pointSelected)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QGLViewer::*)(qglviewer::MouseGrabber * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGLViewer::mouseGrabberChanged)) {
                *result = 12;
                return;
            }
        }
    }
}

const QMetaObject QGLViewer::staticMetaObject = { {
    QMetaObject::SuperData::link<QOpenGLWidget::staticMetaObject>(),
    qt_meta_stringdata_QGLViewer.offsetsAndSize,
    qt_meta_data_QGLViewer,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_QGLViewer_t
, QtPrivate::TypeAndForceComplete<QGLViewer, std::true_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QMouseEvent *, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qglviewer::MouseGrabber *, std::false_type>
, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QColor &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QColor &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qreal, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const qglviewer::Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const qglviewer::Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<const qglviewer::Vec &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qglviewer::Camera * const, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qglviewer::ManipulatedFrame *, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<qglviewer::MouseGrabber *, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<GLint, std::false_type>, QtPrivate::TypeAndForceComplete<GLenum, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<GLint, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QMouseEvent *, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QPoint &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<KeyboardAction, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<KeyboardAction, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButtons, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButtons, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButtons, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButtons, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButtons, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<ClickAction, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButtons, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::Key, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::MouseButton, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<MouseAction, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<Qt::KeyboardModifiers, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<MouseHandler, std::false_type>, QtPrivate::TypeAndForceComplete<unsigned int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QDomElement &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<const QString &, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>


>,
    nullptr
} };


const QMetaObject *QGLViewer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGLViewer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QGLViewer.stringdata0))
        return static_cast<void*>(this);
    return QOpenGLWidget::qt_metacast(_clname);
}

int QGLViewer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QOpenGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 127)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 127;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 127)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 127;
    }
    return _id;
}

// SIGNAL 0
void QGLViewer::viewerInitialized()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QGLViewer::drawNeeded()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QGLViewer::drawFinished(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QGLViewer::animateNeeded()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QGLViewer::helpRequired()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QGLViewer::axisIsDrawnChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QGLViewer::gridIsDrawnChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QGLViewer::FPSIsDisplayedChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QGLViewer::textIsEnabledChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QGLViewer::cameraIsEditedChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QGLViewer::stereoChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void QGLViewer::pointSelected(const QMouseEvent * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void QGLViewer::mouseGrabberChanged(qglviewer::MouseGrabber * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
