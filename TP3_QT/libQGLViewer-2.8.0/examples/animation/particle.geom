#version 430

#extension GL_EXT_geometry_shader4 : enable
#extension GL_EXT_gpu_shader4 : enable

layout ( points ) in;

layout (triangle_strip, max_vertices = 4) out;

// Les matrices peuvent être réutilisées
uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

// Variables d'entrées : des tableaux
in vec3 initialVertPos[];

// Variables de sorties : des éléments uniques
out vec2 UV;


void buildQuad(vec4 position, float scale)
{
	gl_Position = position + scale * vec4(-0.1, -0.1, 0, 0);
	UV = vec2(0, 0); 
	EmitVertex();

	gl_Position = position + scale * vec4(0.1, -0.1, 0, 0);
	UV = vec2(1, 0); 
	EmitVertex();
	
	gl_Position = position + scale * vec4(-0.1, 0.1, 0, 0);
	UV = vec2(0, 1); 
	EmitVertex();
	
	gl_Position = position + scale * vec4(0.1, 0.1, 0, 0);
	UV = vec2(1, 1); 
	EmitVertex();
	EndPrimitive();
}

//Geometry Shader entry point
void main(void) {
	// Taille d'une particule
	float scale = 1;
	buildQuad(gl_in[0].gl_Position, scale);
}
