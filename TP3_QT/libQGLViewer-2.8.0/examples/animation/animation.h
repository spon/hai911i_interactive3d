/****************************************************************************

 Copyright (C) 2002-2014 Gilles Debunne. All rights reserved.

 This file is part of the QGLViewer library version 2.7.2.

 http://www.libqglviewer.com - contact@libqglviewer.com

 This file may be used under the terms of the GNU General Public License 
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain 
 additional rights, described in the file GPL_EXCEPTION in this package.

 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.

 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************************/

#include <QGLViewer/qglviewer.h>
#include <QOpenGLExtraFunctions>
#include <QTime>


class Particle {
public:
  Particle();

  void init();
  void draw();
  void animate();

//private:
  qglviewer::Vec speed_, pos_;
  int age_, ageMax_;
};

class Viewer : public QGLViewer {
protected:
  virtual void draw();
  virtual void init();
  virtual void animate();
  virtual QString helpString() const;

private:
  int nbPart_;
  unsigned int textureId;
  Particle *particle_;

  std::vector<float> particlesPos;
  GLuint particlePosBuffer;
  QTime time;
  float timestamp;

  bool printShaderErrors(GLuint shader);
  bool printProgramErrors(int program);
  bool checkOpenGLError();
  std::string readShaderSource(std::string filename);

  GLuint vShader, gShader, fShader, programID;
  QOpenGLContext* glContext;
  QOpenGLExtraFunctions* glFunctions;

  static void /*GLAPIENTRY */MessageCallback( GLenum source, GLenum type,
                                              GLuint id, GLenum severity,
                                              GLsizei length, const GLchar* message,
                                              const void* userParam );
};
