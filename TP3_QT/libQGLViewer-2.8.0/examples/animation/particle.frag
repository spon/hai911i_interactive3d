#version 430

out vec4 fragColor;

// Input du geometry shader
in vec2 UV;

uniform sampler2D smokeTex;

uniform vec4 color = vec4(1.0, 0.0, 0.0, 1.0);

void main(void)
{
    //fragColor = color;
	
	// Utiliser les coordonnées UV pour débugger : un cadre noir autour des particules
	//if (UV.x < 0.1 || UV.x > 0.9 || UV.y < 0.1 || UV.y > 0.9)
	//{
	//	fragColor = vec4(0.0, 0.0, 0.0, 1.0);
	//}
	
	fragColor = texture(smokeTex, UV);

	if(fragColor.w < 0.6)
		discard;
}
