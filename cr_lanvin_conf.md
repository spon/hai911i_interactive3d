# Compte rendu de conférences
## Génération procédurale d'environnements 3D
Dans un contexte de *recensement* ou d'exploration sous-marine, les scientifiques ont de plus en plus recours à des robots contrôlés à distance ou complètement autonomes.
Ces robots étant assez coûteux il est préférable de les entraîner dans des **simulateurs** pour qu'ils puissent mener à bien leurs missions.

L'objectif est alors de **générer des environnements sous marins** en 3D assez proche de ceux que l'on retrouve dans la vie réelle. Pour cela ils utilisent des **heightmaps** en appliquant du **bruit aléatoire puis de l'érosion**, cette érosion peut être hydraulique, thermique ou encore résultat de l'application de **vent** sur l'environnement.

&rarr; Les heightmaps ne suffisant pas car on peut trouver des grottes sous-marines, il faut donc utiliser des **grilles de voxels** pour les éléments proches et des heightmaps pour les autres (*sorte de niveaux de détails*).

&rarr; Génération trop aléatoire de terrains implique **un grand nombre d'itération** parfois pour obtenir un terrain convenable. Pour pouvoir "contrôler" cette génération aléatoire ils ont eu recours aux **Biotopes**: ce sont des topologies hiérarchisées, ils peuvent donc modifier un biotope dans que cela ait un grand impact sur les autres 

## Comprendre comment le comportement évolue
Pour tenter comprendre plus rigoureusement le comportement des animaux, il faut définir des **métriques**, les scientifiques décomposent ces comportements en plusieurs sous-parties pour pouvoir les **classer quantitativement**.

Il existe une famille de poissons (*les Cichlidés*) dont les espèces ont une structure sociale très éloignées, pour tenter de comprendre pourquoi ces espèces n'ont pas le même comportement alors qu'elles sont de la même famille, les scientifiques ont recours à la **simulation de poissons**.

## Mesurer et analyser les comportements collectifs
Cette fois-ci les scientifiques s'intéressent à la **réalité virtuelle** pour observer le comportement des animaux et essayer de déduire des règles simples de comportements complexes.

Ils se posent aussi la question de comment trouver un modèle 3D qui aurait du sens pour les individus (*fourmis, criquet, poisson ...*), avec lequel on pourrait observer des comportements comme si l'animal était dans son milieu naturel et qu'il voyait un autre animal.

