#ifndef MESH_H
#define MESH_H


#include <vector>
#include <string>
#include "Vec3.h"
#include "Skeleton.h"

#include <cmath>

#include <GL/glut.h>


// -------------------------------------------
// Basic Mesh class
// -------------------------------------------

struct MeshVertex {
    inline MeshVertex () {
        w.clear();
    }
    inline MeshVertex (const Vec3 & _p, const Vec3 & _n) : p (_p), n (_n) {
        w.clear();
    }
    inline MeshVertex (const MeshVertex & vertex) : p (vertex.p), n (vertex.n) , w(vertex.w) {
    }
    inline virtual ~MeshVertex () {}
    inline MeshVertex & operator = (const MeshVertex & vertex) {
        p = vertex.p;
        n = vertex.n;
        w = vertex.w;
        return (*this);
    }
    // membres :
    Vec3 p; // une position
    Vec3 n; // une normale
    std::vector< double > w; // skinning weights
};

struct MeshTriangle {
    inline MeshTriangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline MeshTriangle (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline MeshTriangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    inline virtual ~MeshTriangle () {}
    inline MeshTriangle & operator = (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres :
    unsigned int v[3];
};




class Mesh {
public:
    std::vector<MeshVertex> V;
    std::vector<MeshTriangle> T;

    void loadOFF (const std::string & filename);
    void recomputeNormals ();


    void computeSkinningWeights( Skeleton const & skeleton ) {
        //---------------------------------------------------//
        //---------------------------------------------------//
        // code to change :

        // Indications:
        // you should compute weights for each vertex w.r.t. the skeleton bones
        // so each vertex will have B weights (B = number of bones)
        // these weights shoud be stored in vertex.w:
        int weight_index = 0;
        Vec3 p1, p2, u;
        float t, dist;
        float weight_sum = 0.;

        for( unsigned int i = 0 ; i < V.size() ; ++i ) {
            MeshVertex & vertex = V[ i ];
            vertex.w.resize(skeleton.bones.size());

            for(Bone bone : skeleton.bones) {
                p1 = skeleton.articulations[bone.joints[0]].p;
                p2 = skeleton.articulations[bone.joints[1]].p;

                u = p2 - p1;
                u.normalize();
                t = Vec3::dot(vertex.p - p1, u);

                if(t >= Vec3(p2 - p1).length()) {
                    //pick joint[1] (intersection after the bone)
                    dist = Vec3(vertex.p - p2).length();

                }else if(t <= 0) {
                    //pick joint[0] (intersection before the bone)
                    dist = Vec3(vertex.p - p1).length();

                }else {
                    //pick t * u (intersection inside bone)
                    dist = (vertex.p - (t * u + p1)).length();

                }

                vertex.w[weight_index] = pow((1. / dist), 7);
                weight_sum += vertex.w[weight_index];
                weight_index ++;

            }

            for(int j = 0; j < skeleton.bones.size(); j ++) {
                V[i].w[j] /= weight_sum;
            }

            weight_sum = 0;
            weight_index = 0;
        }
        

        //---------------------------------------------------//
        //---------------------------------------------------//
        //---------------------------------------------------//
    }

    void draw(int displayedBone) const {
        glEnable(GL_LIGHTING);
        glBegin (GL_TRIANGLES);
        for (unsigned int i = 0; i < T.size (); i++)
            for (unsigned int j = 0; j < 3; j++) {
                const MeshVertex & v = V[T[i].v[j]];
                glNormal3f (v.n[0], v.n[1], v.n[2]);
                glVertex3f (v.p[0], v.p[1], v.p[2]);
                glColor3f  (0, v.w[displayedBone], 0);
            }
        glEnd ();
    }

    void drawTransformedMesh( SkeletonTransformation const & transfo ) const {
        std::vector< Vec3 > newPositions( V.size() );

        //---------------------------------------------------//
        //---------------------------------------------------//
        // code to change :
        int bone_size = V[0].w.size();

        for( unsigned int i = 0 ; i < V.size() ; ++i ) {
            Vec3 p = Vec3(0, 0, 0);

            // Indications:
            // you should use the skinning weights to blend the transformations of the vertex position by the bones.
            
            for(int j = 0; j < bone_size; j ++) {
                Mat3 rot = transfo.boneTransformations[j].worldSpaceRotation;
                p += V[i].w[j] * ( rot*V[i].p + transfo.boneTransformations[j].worldSpaceTranslation);
            }
            newPositions[ i ] = p;
        }
        //---------------------------------------------------//
        //---------------------------------------------------//
        //---------------------------------------------------//

        glEnable(GL_LIGHTING);
        glBegin (GL_TRIANGLES);
        for (unsigned int i = 0; i < T.size (); i++)
            for (unsigned int j = 0; j < 3; j++) {
                const MeshVertex & v = V[T[i].v[j]];
                Vec3 p = newPositions[ T[i].v[j] ];
                glNormal3f (v.n[0], v.n[1], v.n[2]);
                glVertex3f (p[0], p[1], p[2]);
            }
        glEnd ();
    }
};



#endif
