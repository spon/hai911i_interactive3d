# TP1 Animation

## Question 1

Ajout de ``  glColor3f  (0, v.w[*displayedBone*], 0);`` dans la fonction de **draw** des **meshes**, cela permet de visualiser le poids des sommets en fonction de l'os selectionné.

| ![](pngs/weights.png) | ![](pngs/weights_2.png) |
| ---------------- | ------------------ |

## Question 2

 Puisque le poids de l'os j sur le sommet i est définit comme: $ w_{ij} = (\frac{1}{dist_{ij}})^n$  et que la **fonction de déformation** est définie comme une somme des poids de tous les os sur le sommet: $f(v_i) = \sum_jw_{ij}(...)$
 on voit qu'en augmentant $n$ ce poids diminue et alors le maillage aura tendance à être moins déformé.

On peut alors remarquer que pour un petit $n$, lors de la déformation de la queue du maillage, les bras et la tête sont aussi légèrement déformés.

*Sur ces captures d'écran, $n=8$*.

| <img src="pngs/anim_3.png" style="zoom:33%;" /> | <img src="pngs/anim_4.png" style="zoom:33%;" /> |
| ------------------------------------------ | :----------------------------------------: |

